require('luasvm')

math.randomseed( os.time() )

local data   = {}
local labels = {}

for i = 1, 1000 do
    local tbl = {}
    local acc = 0
    for j = 1, 3 do
        tbl[j] = math.random(-3, 3)
        acc    = acc + tbl[j]
    end
    table.insert(data,   tbl)
    table.insert(labels, acc < 0 and -1 or 1)
end

local svm    = luasvm.new()
local opt    = { kernel = luasvm.linear, tol = 1e-4, C = 10 }
local iters  = svm:train(data, labels, opt)
print("Training iterations: " .. iters)

local acc = 0
for i = 1, #data do
    local l = labels[i]
    local p = svm:predict(data[i])
    if l == p then acc = acc + 1 end
end
print( string.format("Classification rate: %.2f%%", (acc*100) / #data) )
