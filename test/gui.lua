#! /usr/bin/env lua

require('luasvm')
require('lgob.gtk')
require('lgob.gdk')
require('lgob.cairo')

math.randomseed( os.time() )

local data   = { {0, 0}, {0, 1}, {1, 0}, {1, 1} }
local labels = { -1, 1, 1, -1 }
local svm    = luasvm.new()
local opt    = { kernel = luasvm.make_rbf(0.5), tol = 1e-4, C = 5 }
local area   = nil

local SIZE   = 400
local FACT   = SIZE / 4

local function get_kernel(id)
    if     id == 0 then
        return luasvm.linear
    elseif id == 1 then
        return luasvm.make_poly(2,1)
    end

    return luasvm.make_rbf(0.5)
end

local function train_svm()
    local iters  = svm:train(data, labels, opt)
    print(iters .. ' iters')
    area:queue_draw()
end

local function conv(p)
    return (p + 2) * FACT
end

local function add_vector(v, l)
    table.insert(data  , v)
    table.insert(labels, l)
    train_svm()
end

local function inv(p)
    return (p / FACT) - 2
end

local function scale(a)
    a = math.log(a * 100)
    if a < 2 or a ~= a then return 2 else return a end
end

local function draw_vector(cr, v, l, a)
    if l == -1 then
        cr:set_source_rgb(0, 0, 1)
    else
        cr:set_source_rgb(1, 0, 0)
    end
    
    cr:arc( conv(v[1]), conv(-v[2]), scale(a), 0, 2 * math.pi)
    cr:fill_preserve()
    cr:set_source_rgb(0,0,0)
    cr:set_line_width(3)
    cr:stroke()
end

local function draw_prediction(cr, x1, x2, w)
    local l = svm:predict({x1,x2})
    if l == -1 then
        cr:set_source_rgb(0.3, 0.3, 1)
    else
        cr:set_source_rgb(1, 0.3, 0.3)
    end
    
    local x = conv(x1) - w / 2
    local y = conv(-x2) - w / 2
    cr:rectangle(x, y, w * FACT, w * FACT)
    cr:fill()
end

local function draw(widget, cr)
    cr = cairo.Context.wrap(cr)

    -- Draw predictions
    local w = 0.02
    for x1 = -2, 2, w do
        for x2 = -2, 2, w do
            draw_prediction(cr, x1, x2, w)
        end
    end

    -- Draw axis
    cr:set_source_rgb(0, 0, 0)
    cr:set_line_width(2)
    cr:move_to    (0, SIZE / 2)
    cr:rel_line_to(SIZE, 0)
    cr:move_to    (SIZE/2, 2)
    cr:rel_line_to(0, SIZE)
    cr:stroke()
    
    -- Draw vectors
    for i = 1, #data do
        draw_vector(cr, data[i], labels[i], svm.alpha[i])
    end
    
    return false
end

-- main window
local window = gtk.Window.new()
window:set_resizable(false)
window:set('title', "luasvm demo", 'window-position', gtk.WIN_POS_CENTER)

-- layout
local grid   = gtk.Grid.new()
grid:set_column_spacing(10)

-- drawing area
area   = gtk.DrawingArea.new()
area:set('width-request', SIZE, 'height-request', SIZE)
area:add_events(gdk.BUTTON_PRESS_MASK)
area:connect('button-press-event', function(_, event)
    local _, x, y, _, b = gdk.event_button_get(event)
    local v             = {inv(x), inv(SIZE-y)}
    add_vector(v, b <= 1 and -1 or 1)
end)
grid:attach(area, 0, 0, 4, 1)

-- kernel selection
local klabel = gtk.Label.new_with_mnemonic('_Kernel')
klabel:set_xalign(1)
local ksel   = gtk.ComboBoxText.new()
ksel:append('linear', 'Linear'               )
ksel:append('poly'  , 'Polynomial (2º order)')
ksel:append('rbf'   , 'Radial Basis Function')
ksel:set_active(2)
klabel:set_mnemonic_widget(ksel)
grid:attach(klabel, 0, 1, 1, 1)
grid:attach(ksel  , 1, 1, 3, 1)

-- C parameter selection
local clabel = gtk.Label.new_with_mnemonic('_C')
clabel:set_xalign(1)
local csel   = gtk.SpinButton.new_with_range(0, 1e4, 0.1)
csel:set_value(opt.C)
clabel:set_mnemonic_widget(csel)
grid:attach(clabel, 0, 2, 1, 1)
grid:attach(csel  , 1, 2, 3, 1)

-- train button
local function conf_train()
    local K = ksel:get_active()
    local C = csel:get_value()
    
    opt.C      = C
    opt.kernel = get_kernel(K)
    
    train_svm()
end

local tbutton = gtk.Button.new_with_mnemonic('Configure and _Train')
tbutton:connect('clicked', conf_train)
grid:attach(tbutton, 0, 3, 2, 1)

-- reset button
local rbutton = gtk.Button.new_with_mnemonic('_Reset')
rbutton:connect('clicked', function()
    ksel:set_active(2)
    csel:set_value (5)
    data, labels = {}, {}
    conf_train()
end)
grid:attach(rbutton, 2, 3, 2, 1)

-- connect events and show main window
window:add(grid)
window:show_all()
window:connect('delete-event', gtk.main_quit)
area:connect('draw', draw, area)

train_svm()
gtk.main()
