require('luasvm')

math.randomseed( os.time() )

local data, labels = luasvm.data.load_file('test/nonlinear.data')
local svm    = luasvm.new()
local opt    = { kernel = luasvm.make_poly(2,0), tol = 1e-4, C = 1000 }
local iters  = svm:train(data, labels, opt)
print("Training iterations: " .. iters)

local acc = 0
for i = 1, #data do
    local l = labels[i]
    local p = svm:predict(data[i])
    if l == p then acc = acc + 1 end
end
print( string.format("Classification rate: %.2f%%", (acc*100) / #data) )
