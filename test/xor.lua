require('luasvm')

math.randomseed( os.time() )

local data, labels = luasvm.data.load_libsvm('test/xor.data')
local svm          = luasvm.new()
local opt          = { kernel = luasvm.make_rbf(1), tol = 1e-4, C = 10 }
local iters        = svm:train(data, labels, opt)
print("Training iterations: " .. iters)

for i = 1, #data do
    print( string.format('Label: %2d | Prediction: %2d', labels[i], svm:predict(data[i])) )
end
