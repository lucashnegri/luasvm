local data   = {}
local datamt = {__index = data}

function data.load_file(path)
    local self   = {}
    local labels = {}
    setmetatable(self, datamt)

    local f    = assert( io.open(path) )
    local N, D = f:read('*n'), f:read('*n')

    for i = 1, N do
        local label = f:read('*n')
        labels[i]   = label

        local t = {}
        for j = 1, D do
            t[j] = f:read('*n')
        end

        self[i] = t
    end

    f:close()

    return self, labels
end

function data:save_file(labels, path)
    local f = assert( io.open(path, 'w') )
    
    local N, D = #self, #self[1]
    f:write( string.format('%d %d\n', N, D) )
    
    for n = 1, N do
        f:write(labels[n])
        for d = 1, D do
            f:write(string.format(' %d', self[n][d]))
        end
        f:write('\n')
    end
    
    f:close()
end

function data.load_libsvm(path)
    local self   = {}
    local labels = {}
    setmetatable(self, datamt)
    
    local f    = assert( io.open(path) )
    local N, D = f:read('*n'), f:read('*n')
    
    for i = 1, N do
        local label = f:read('*n')
        labels[i]   = label
        
        local t = {}
        for j = 1, D do t[j] = 0 end
        local l = f:read('*l')
        
        for a, b in l:gmatch('(.?%d):(.?%d)') do
            local a = tonumber(a)
            local b = tonumber(b)
            t[a] = b
        end
        
        self[i] = t
    end
    
    f:close()
    
    return self, labels
end

return data
