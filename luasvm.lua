-- implements the simplified SMO algorithm
luasvm          = {}
local luasvmmt  = {__index = luasvm}

local function zeros(N)
    local tbl = {}
    for i = 1, N do tbl[i] = 0 end
    return tbl
end

local function clip(v, l, h)
    if v < l then
        return l
    elseif v > h then
        return h
    end
    return v
end

function luasvm.new()
    local self = {}
    setmetatable(self, luasvmmt)
    
    return self
end

function luasvm:train(data, labels, options)
    local options    = options          or {}
    local C          = options.C        or 10
    local tol        = options.tol      or 1e-4
    local maxiters   = options.maxiters or 10000
    local kernel     = options.kernel   or svm.linear
    local numpass    = options.numpass  or 10
    local N, D       = #data, data[1] and #data[1] or 0
    local alpha      = zeros(N)
    local iter, pass = 0, 0
    
    self.N           = N
    self.data        = data
    self.labels      = labels
    self.alpha       = alpha
    self.b           = 0
    self.kernel      = kernel
    
    if N <= 1 or D == 0 then return 0 end
    
    while iter < maxiters and pass < numpass do
        local alpha_changed = 0
        
        for i = 1, N do
            local Ei    = self:margin(data[i]) - labels[i]
            local cond1 = labels[i] * Ei < -tol and self.alpha[i] < C
            local cond2 = labels[i] * Ei >  tol and self.alpha[i] > 0
            
            if cond1 or cond2 then
                -- update alpha_i. Picking an alpha_j
                local j  = i
                while j  == i do j = math.random(1, N) end
                local Ej = self:margin(data[j]) - labels[j];
                
                -- calculate L and H bounds for j to ensure we're in [0 C]x[0 C] box
                local ai, aj = self.alpha[i], self.alpha[j]
                local L, H   = 0, C
                
                if labels[i] == labels[j] then
                    L = math.max(0, ai + aj - C)
                    H = math.min(C, ai + aj)
                else
                    L = math.max(0, aj - ai)
                    H = math.min(C, C + aj - ai)
                end
                
                if math.abs(L - H) >= tol then
                    local eta = 2 * kernel(data[i], data[j]) - kernel( data[i], data[i]) - kernel(data[j], data[j])
                    if eta < 0 then
                        -- compute new alpha_j and clip it inside [0 C]x[0 C] box then compute alpha_i based on it.
                        local newaj = clip(aj - labels[j]*(Ei - Ej) / eta, L, H)
                        if math.abs(aj - newaj) >= tol then
                            self.alpha[j] = newaj
                            local newai   = ai + labels[i]*labels[j]*(aj - newaj)
                            self.alpha[i] = newai
                            
                            local aux1    = labels[i] * (newai - ai)
                            local aux2    = labels[j] * (newaj - aj)
                            
                            local b1 = self.b - Ei - aux1 * kernel(data[i], data[i]) - aux2 * kernel(data[i], data[i])
                            local b2 = self.b - Ej - aux1 * kernel(data[i], data[j]) - aux2 * kernel(data[j], data[j])
                            self.b = (b1 + b2) / 2
                            if newai > 0 and newai < C then self.b = b1 end
                            if newaj > 0 and newaj < C then self.b = b2 end
                            alpha_changed = alpha_changed + 1
                        end
                    end
                end
            end
        end
        
        iter = iter + 1
        if alpha_changed == 0 then
            pass = pass + 1
        else
            pass = 0
        end
    end
    
    return iter
end

function luasvm.linear(v1, v2)
    local acc = 0
    for i = 1, #v1 do
        acc = acc + v1[i] * v2[i]
    end
    return acc
end

function luasvm.make_rbf(sigma)
    return function(v1, v2)
        local acc = 0
        for i = 1, #v1 do
            local aux = v1[i] - v2[i]
            acc = acc + aux*aux
        end
        return math.exp(-acc / (2*sigma*sigma))
    end
end

function luasvm.make_poly(order, c)
    return function(v1, v2)
        local acc = 0
        for i = 1, #v1 do
            acc = acc + v1[i] * v2[i]
        end
        return (acc + c)^order
    end
end

function luasvm.make_tanh(b1, b2)
    return function(v1, v2)
        local acc = 0
        for i = 1, #v1 do
            acc = acc + v1[i] * v2[i]
        end
        return math.tanh(b1*acc + b2)
    end
end

function luasvm:margin(v)
    local m = self.b
    
    for i = 1, self.N do
        m = m + self.alpha[i] * self.labels[i] * self.kernel(v, self.data[i])
    end
    
    return m
end

function luasvm:predict(v)
    return self:margin(v) < 0 and -1 or 1
end

luasvm.data = require('luasvm_data')
