# luasvn

## About

luasvm is a Lua library that implements the simplified version of the SMO algorithm for
training SVMs.

luasvm is licensed under the MIT license and can be freely used for
academic and commercial purposes.

## Install

lua-gnuplot is a pure Lua library. To install it, copy luasvm.lua and luasvm_data.lua to into your
Lua shared folder (eg, /usr/share/lua/5.2).

I recommend using with luajit (for a 100x increase in performance).

## Source code and Contact

Repository on [bitbucket][1].
Contact me at lucashnegri@gmail.com.

[1]: https://bitbucket.org/lucashnegri/luasvm
